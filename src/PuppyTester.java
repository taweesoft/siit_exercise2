
public class PuppyTester {
	public static void main(String[] args){
		Puppy tommy = new Puppy("Tommy");
		tommy.setOwner("John Moore");
		tommy.setAge(2);
		Puppy bobby = new Puppy("Bobby","Michael Johnson",3);
		Puppy emmy = new Puppy("Emmy","Linsey Lohan",1);
		
		System.out.println("Puppy name: " + tommy.puppyName);
		System.out.println("Age: "+ tommy.getAge());
		System.out.println("Owner: " + tommy.getOwner());
		System.out.println();
		System.out.println("Puppy name: " + bobby.puppyName);
		System.out.println("Age: "+ bobby.getAge());
		System.out.println("Owner: " + bobby.getOwner());
		System.out.println();
		System.out.println("Puppy name: " + emmy.puppyName);
		System.out.println("Age: "+ emmy.getAge());
		System.out.println("Owner: " + emmy.getOwner());
		
	}
}
