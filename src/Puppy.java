
public class Puppy {
	String puppyName;
	private String puppyOwner;
	private int puppyAge;
	public Puppy(String name,String owner,int age){
		puppyName = name;
		puppyOwner = owner;
		puppyAge = age;
	}
	public Puppy(String name){
		puppyName = name;
		puppyOwner = "";
		puppyAge = 0;
	}
	public String getOwner(){
		return puppyOwner;
	}
	public void setOwner(String owner){
		puppyOwner = owner;
	}
	public int getAge(){
		return puppyAge;
	}
	public void setAge(int age){
		puppyAge = age;
	}
}
